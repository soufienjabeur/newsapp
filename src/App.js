import React, { useState, useEffect } from 'react';
import alanBtn from "@alan-ai/alan-sdk-web";
import NewsCards from './components/NewsCards/NewsCards';

import useStyles from './styles.js';
const alanKey = '7d735db8b5127c6d47b7b69142ae4a4d2e956eca572e1d8b807a3e2338fdd0dc/stage';


const App = () => {
    const [newArticles, setNewsArticles] = useState([])
    const classes = useStyles();
    useEffect(() => {
        alanBtn({
            key: alanKey,
            onCommand: ({ command, articles }) => {
                if (command === 'newHeadlines') {
                    setNewsArticles(articles);
                }
            }
        })
    }, [])
    return (
        <div className={classes.container}>
            <div className={classes.logoContainer}>
                <img src="https://alan.app/voice/images/previews/preview.jpg" className={classes.alanLogo} alt="alan logo" />
            </div>
            <NewsCards articles={newArticles} />
        </div>
    )
}

export default App;