import React from 'react'
import NewsCard from '../NewsCard/NewsCard';
import { Grid, Grow, Typography } from '@material-ui/core'
import useStyles from './styles'

const NewsCards = ({ articles }) => {

    const classes = useStyles();
    const infoCard = [
        { color: '#00838f', title: 'Lastest News', text: 'Give me the lastest news' },
        { color: '#1565c0', title: 'News by categories', info: 'Business, Entretainment, General, Health, Science, Sport, Technology', text: 'Give me the lastest Technology news' },
        { color: '#4527a0', title: 'News by Items', info: 'Bitcoin, PlayStation 5, Smartphone ...', text: 'What\'s up with PLayStation 5' },
        { color: '#283593', title: 'News by Sources', info: 'CNN, Wired, BBC News, Time, IGN, ABC News', text: 'Give me the news from CNN' }
    ]

    if (!articles.length) {
        return (
            <Grow in>
                <Grid className={classes.container} container alignItems="stretch" spacing={3}>
                    {infoCard.map((infoCard) => (
                        <Grid item xs={12} sm={6} md={4} lg={3} className={classes.infoCard}>
                            <div className={classes.card} style={{ backgroundColor: infoCard.color }}>
                                <Typography variant="h5">{infoCard.title}</Typography>
                                {infoCard.info ? (<Typography variant="body1"><strong>{infoCard.title.split(' ')[2]}: <br />{infoCard.info}</strong></Typography>) : null}
                                <Typography varaint="h6"> Try saying: <br /><i>{infoCard.text}</i></Typography>
                            </div>
                        </Grid>

                    ))}
                </Grid>
            </Grow>
        );

    }
    return (
        <Grow in>
            <Grid className={classes.container} container alignItems="stretch" spacing={3}>
                {articles.map((article, i) => (
                    <Grid item xs={12} sm={6} md={4} lg={3} style={{ display: 'flex' }}>
                        <NewsCard article={article} i={i} />
                    </Grid>

                ))}
            </Grid>

        </Grow>
    )
}

export default NewsCards
