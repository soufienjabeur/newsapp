## Project steps

### Client side

- Createa React App using command `npx create-react-app posts_app`
- Install the `npm install @alan-ai/alan-sdk-web package`
- import the `npm install alanBtn` into the App component
- Add the Alan button to the App component
- Install Material UI `npm install @material-ui/core`
- Create new folder `components` inside of it, create two folders:
  - `NewsCards`: inside of it, create two files `NewsCards.js` and `style.js`
  - `NewsCard`: inside of it, create two files `NewsCard.js` and `style.js`
- add few SEO rules using `meta-tags` such as:
  - adding description to the app
  - keyWords Attribute
  - Charset
  - Author
- Deploy the News app on Netlify

#### Hooks: `useEffect()`, `useState()`, `useStyles()`

### Alan sdk side

- Sign up for the Alan Studio
- Create voice scripts, or conversation scenarios, for the voice assistant

### UseFul Links:

[https://alan.app/docs/usage/getting-started](https://alan.app/docs/usage/getting-started)

[https://alan.app/docs/client-api/web/react](https://alan.app/docs/client-api/web/react)

[https://www.netlify.com/](https://www.netlify.com/)
